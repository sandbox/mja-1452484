Readme file for the EMF GraphicMail module for Drupal
---------------------------------------------

EMF GraphicMail is a plugin for the Email Marketing Framework module
which allows sites that use the e-newsletter mailing platform GraphicMail
to maintain synchronised subscriber lists with Drupal.

It requires a working install of the Email Marketing Framework,
a GraphicMail account, and GraphicMail API access for your server.

Installation:
  Install the EMF GraphicMail module into the plugins directory
  of the EMF module folder (typically sites/all/modules/emf/plugins).

Dependencies:
  The EMF GraphicMail module is dependent on the EMF module.

Configuration:
  The configuration page is at admin/settings/emf_graphicmail,
  where you can enter your API details.
  
  Further options are available under admin/build/emf/list. You must run cron
  before your lists will appear in Drupal.
