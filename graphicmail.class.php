<?php

/**
 * @file
 * Graphicmail API PHP wrappers.
 *
 * @author Mark Anslow (mark@ethicaldigital.co.uk)
 */

class Graphicmail {

  /**
   * API username
   * @var
   */
  protected $username;

  /**
   * API password
   * @var
   */
  protected $password;

  /**
   * API standard identifier
   * @var
   */
  protected $SID;

  /**
   * Graphicmail API stem as of 08/2011
   * @var
   */
  protected $host = "www.graphicmail.co.uk";

  /**
   * Graphicmail api location as of 08/2011
   * @var
   */
  protected $apiLoc = "/api.aspx";

  /**
   * Set up credentials for API use
   *
   * @param String $username
   *   API username
   * @param String $password
   *   API password
   */
  public function __construct($username, $password) {
    $this->username = array('key' => 'Username', 'value' => $username);
    $this->password = array('key' => 'Password', 'value' => $password);
    $this->SID = array('key' => 'SID', 'value' => '6');
    libxml_use_internal_errors(TRUE);
  }

  /**
   * Subscribe a user to a mailing list.
   *
   * @param String $email
   *   Email address
   * @param Int $lid
   *   List ID
   * @param Array $custom_fields
   *   Custom fields for linked dataset
   *
   * @return Boolean
   *   TRUE on success; FALSE on failure
   */
  public function subscribeUser($email, $lid, $custom_fields, $dataset_id = NULL) {
    // GraphicMail API has a bug that doesn't allow it to accept '+' in emails.
    // To avoid this, double url encode it.
    $email = str_replace('+', '%252B', $email);
    $params = array(
      array(
        'key' => 'Function',
        'value' => 'post_subscribe',
      ),
      array(
        'key' => 'Email',
        'value' => $email,
      ),
      array(
        'key' => 'MailinglistID',
        'value' => $lid,
      ),
    );
    $subscribe_response = preg_match('/^[12]/', $this->apiCall($params)) == 1 ? TRUE : FALSE;

    // If there are custom field data, post them now.
    if (!empty($custom_fields) && !empty($dataset_id)) {
      $custom_fields_response = preg_match('/^[12]/', $this->setCustomFields($email, $lid, $dataset_id, $custom_fields)) == 1 ? TRUE : FALSE;
      if (!$custom_fields_response) {
        watchdog('emf_graphicmail', 'Error adding custom fields data for subscriber @email', array('@email' => $email));
      }
    }

    return $subscribe_response;
  }

  /**
   * Unsubscribe a user from a mailing list.
   *
   * @param String $email
   *   Email address
   * @param Int $lid
   *   List ID
   *
   * @return Mixed
   *   Response from API
   */
  public function unsubscribeUser($email, $lid, $dataset_id) {
    $params = array(
      array(
        'key' => 'Function',
        'value' => 'post_unsubscribe',
      ),
      array(
        'key' => 'Email',
        'value' => $email,
      ),
      array(
        'key' => 'MailinglistID',
        'value' => $lid,
      ),
    );
    $unsubscribe_response = preg_match('/^[12]/', $this->apiCall($params)) == 1 ? TRUE : FALSE;
    if (!empty($dataset_id)) {
      $params = array(
        array(
          'key' => 'Function',
          'value' => 'post_delete_from_dataset',
        ),
        array(
          'key' => 'Email',
          'value' => $email,
        ),
        array(
          'key' => 'DatasetID',
          'value' => $dataset_id,
        ),
      );
      $remove_custom_fields_response = preg_match('/^[1]/', $this->apiCall($params)) == 1 ? TRUE : FALSE;
      if (!$remove_custom_fields_response) {
        watchdog('emf_graphicmail', 'Could not remove dataset data for subscriber ' . $email . ' from dataset ' . $dataset_id . '. The user may not have had a dataset entry');
      }
    }
    return $unsubscribe_response;
  }

  /**
   * Get mailing lists.
   *
   * @return Array
   *   List of mailing lists.
   */
  public function getLists() {
    $params = array('key' => 'Function', 'value' => 'get_mailinglists');
    $return_array = array();
    foreach ($this->apiCall(array($params), 'mailinglist') as $mailinglist) {
      $return_array[] = array('lid' => (string) $mailinglist->mailinglistid, 'name' => (string) $mailinglist->description);
    }
    return $return_array;
  }

  /**
   * Get datasets.
   *
   * @return Array
   *   List of datasets.
   */
  public function getDatasets() {
    $params = array('key' => 'Function', 'value' => 'get_datasets');
    $return_array = array();
    foreach ($this->apiCall(array($params), 'dataset') as $dataset) {
      $return_array[] = array('id' => (string) $dataset->datasetid, 'name' => (string) $dataset->name);
    }
    return $return_array;
  }

  /**
   * Get active subscribers.
   *
   * @param Int $lid
   *   List ID
   *
   * @return Array
   *   List of subscribers
   */
  public function getSubscribedSubscribers($lid) {
    $params = array(
      array(
        'key' => 'Function',
        'value' => 'get_mailinglist',
      ),
      array(
        'key' => 'MailinglistID',
        'value' => $lid),
    );
    $all_signups = $this->apiCall($params, 'email', TRUE);
    $return_array = array();
    if (!empty($all_signups) && is_array($all_signups)) {
      foreach ($all_signups as $signup) {
        if ($signup->status != 'U') {
          $return_array[] = (string) $signup->emailaddress;
        }
      }
    }
    return $return_array;
  }

  /**
   * Get unsubscribed subscribers.
   *
   * @param Int $lid
   *   List ID
   *
   * @return Array
   *   List of unsubscribed subscribers
   */
  public function getUnsubscribedSubscribers($lid) {
    $params = array(
      array(
        'key' => 'Function',
        'value' => 'get_mailinglist',
      ),
      array(
        'key' => 'MailinglistID',
        'value' => $lid,
      ),
      array(
        'key' => 'Status',
        'value' => 'U',
      ),
    );
    $unsubscribers = $this->apiCall($params, 'email', TRUE);
    $return_array = array();
    if (!empty($unsubscribers) && is_array($unsubscribers)) {
      foreach ($unsubscribers as $unsubscriber) {
        $return_array[] = (string) $unsubscriber->emailaddress;
      }
    }
    return $return_array;
  }

  /**
   * Get the fields for a given dataset.
   *
   * @param Int $dataset_id
   *   Dataset ID
   *
   * @return Array
   *   Array of fields
   */
  public function getCustomFields($dataset_id) {
    $params = array(
      array(
        'key' => 'Function',
        'value' => 'get_dataset_columns',
      ),
      array(
        'key' => 'DatasetID',
        'value' => $dataset_id),
    );
    if ($xml = simplexml_load_string($this->apiCall($params))) {
      $object_as_array = get_object_vars($xml);
      unset($object_as_array['datasetid']);
      unset($object_as_array['name']);
      $return_array = array();
      foreach ($object_as_array as $key => $name) {
        $return_array[] = array('key' => $key, 'name' => $name);
      }
      return $return_array;
    }
    else {
      throw new Exception("Couldn't parse XML returned for dataset columns");
    }
  }

  /**
   * Post custom field data to Graphicmail dataset.
   *
   * @param String $email
   *   Email address of user
   * @param Int $lid
   *   List ID.
   * @param Int $dataset_id
   *   Dataset id.
   * @param Array $dataset
   *   Data for dataset, keyed by column name
   *
   * @return String
   *   Response from API
   */
  protected function setCustomFields($email, $lid, $dataset_id = NULL, $dataset = NULL) {
    $params = array(
      array(
        'key' => 'Function',
        'value' => 'post_insertdata',
      ),
      array(
        'key' => 'Email',
        'value' => $email,
      ),
      array(
        'key' => 'DatasetID',
        'value' => $dataset_id),
    );
    $arrayed_dataset = array();
    foreach ($dataset as $dataset_element_key => $dataset_element_value) {
      $arrayed_dataset[] = array('key' => $dataset_element_key, 'value' => urlencode($dataset_element_value));
    }
    $complete_params = array_merge($params, $arrayed_dataset);
    return $this->apiCall($complete_params);
  }

  /**
   * Get a list of newsletters from Graphicmail.
   *
   * @return Array
   *   Newsletters with names and ids
   */
  public function getNewsletters() {
    $params = array(array('key' => 'Function', 'value' => 'get_newsletters'));
    $arrayed_newsletters = array();
    foreach ($this->apiCall($params, 'newsletter') as $newsletter) {
      $arrayed_newsletters[] = (array) $newsletter;
    }
    return $arrayed_newsletters;
  }

  /**
   * Post newsletter to Graphicmail.
   *
   * @param String $path
   *   Path to the html and text files, including basename
   *
   * @return String
   *   Response from API
   */
  public function postNewsletter($path, $filename) {
    // Check to see if newsletter already exists in GraphicMail database.
    $newsletters = $this->getNewsletters();
    $newsletters_names_ids = array();
    $newsletter_names = array();
    foreach ($newsletters as $newsletter) {
      $newsletter_names[$newsletter['newslettername']] = $newsletter['newsletterid'];
    }
    $newsletter_exists = array_key_exists($filename, $newsletter_names);

    $params = array(
      array(
        'key' => 'Function',
        'value' => 'post_import_newsletter',
      ),
      array(
        'key' => 'HtmlURL',
        'value' => urlencode($path . $filename . '.html'),
      ),
      array(
        'key' => 'NewExisting',
        'value' => $newsletter_exists ? 'Existing' : 'New',
      ),
      array(
        'key' => 'TextURL',
        'value' => urlencode($path . $filename . '.txt'),
      ),
    );

    if ($newsletter_exists) {
      $params[] = array(
        'key' => 'NewsletterID',
        'value' => $newsletter_names[$filename],
      );
    }
    else {
      $params[] = array('key' => 'NewsletterName', 'value' => urlencode($filename));
    }
    return $this->apiCall($params);
  }

  /**
   * Make a call to the Graphicmail API.
   *
   * @param Array $params
   *   Parameters to pass to caller methods
   * @param String $xpath
   *   An xpath element to seek in returned XML
   * @param Boolean $allow_string
   *   Flag to indicate whether an xpath can be given
   *   yet a string returned
   *
   * @return Mixed
   *   Either an array or string
   */
  protected function apiCall($params, $xpath = NULL, $allow_string = FALSE) {
    // If $xpath is set we're expecting XML, unless $allow_string is set.
    if ($xpath) {
      $raw_api_data = $this->fsockRequest($params, $xpath);

      // Base64 string is the xml start tag in case php short tags are enabled.
      if (strpos($raw_api_data, base64_decode('PD94bWw=')) !== FALSE) {
        return $this->parseXML($raw_api_data, $xpath);
      }
      elseif ($allow_string) {
        return $raw_api_data;
      }
      else {
        throw new Exception("Couldn't parse XML returned from GraphicMail API: " . $raw_api_data);
      }
    }
    // Any request without an xpath is only expecting a string to be returned.
    else {
      return $this->fsockRequest($params);
    }
  }

  /**
   * Parse XML according to xPath and return an array.
   *
   * @param String $data
   *   XML to process.
   * @param String $xpath
   *   XML path to search for.
   *
   * @return Array
   *   Xpath array
   */
  protected function parseXML($data, $xpath) {
    if ($xml = new SimpleXMLElement($data)) {
      return $xml->xpath($xpath);
    }
    else {
      $error_string = "";
      foreach (libxml_get_errors() as $error) {
        $error_string .= $error . " :: ";
      }
      throw new Exception("Invalid XML returned by GraphicMail API: " . $error_string);
    }
  }

  /**
   * Produce a key=value pair from a simple array.
   *
   * @param Array $param
   *   An array containing one key ['key']
   *   and one key ['value']
   * @param Boolean $add_ampersand
   *   Flag to indicate whether an ampersand
   *   should be suffixed to the key=value pair
   *
   * @return String
   *   Parameter string
   */
  protected function parametize($param, $add_ampersand = TRUE) {
    $returnstring = $param['key'] . "=" . $param['value'];
    $returnstring .= $add_ampersand ? '&' : '';
    return $returnstring;
  }

  /**
   * Make an API call using fsockopen.
   *
   * @param Array $params
   *   A multidimensional array of parameters for the call.
   *
   * @return Mixed
   *   XML or String
   */
  protected function fsockRequest($params) {

    // Prepare parameters.
    $parameters = '';
    $parameters .= $this->parametize($this->username);
    $parameters .= $this->parametize($this->password);
    foreach ($params as $param) {
      $parameters .= $this->parametize($param);
    }
    $parameters .= $this->parametize($this->SID, FALSE);

    // Attempt to open socket.
    $fp = fsockopen("ssl://" . $this->host, 443, $errno, $errstr, 30);

    // If socket has opened successfully, proceed.
    if (!$fp) {
      return "$errstr ($errno)<br />\n";
    }
    else {
      // Make call.
      $out = "GET " . $this->apiLoc . '?' . $parameters . " HTTP/1.0\r\n";
      $out .= "Host: www.graphicmail.co.uk\r\n";
      $out .= "Connection: Close\r\n\r\n";
      fwrite($fp, $out);
      $data = "";
      while (!feof($fp)) {
        $data .= fgets($fp);
      }
      fclose($fp);

      // Look for XML and extract if found.
      $xml_start = strpos($data, base64_decode('PD94bWw='));
      if ($xml_start !== FALSE) {
        $xml_length = strlen($data) - $xml_start;
        $xml = substr($data, $xml_start, $xml_length);
        return $xml;
      }

      // If no XML has been returned, look for a one line response code.
      elseif (preg_match('/\r\n\r\n(\d\|.*?)$/', $data, $matches)) {
        return $matches[1];
      }

      // If response is not parseable, throw error and return HTTP code.
      else {
        preg_match('/HTTP\/\d\.\d\s(\d\d\d)/', $data, $http_response_code);
        throw new Exception("Couldn't parse data returned from Graphicmail: " . $http_response_code[1]);
      }
    }
  }
}
