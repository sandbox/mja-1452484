<?php

/**
 * @file
 * Graphicmail API call wrappers
 *
 * @author Mark Anslow
 */

require_once 'graphicmail.class.php';

/**
 * Subscribe a user to a list.
 *
 * @param String $email
 *   E-mail address to subscribe.
 * @param Array $fields
 *   Array of custom field values.
 *   Key is field. Value is value for the field.
 * @param String $lid
 *   List ID of the list to subscribe to.
 *
 * @return Boolean
 *   TRUE if user is subscribed. FALSE if not.
 */
function emf_graphicmail_api_subscribe($email, $fields, $lid) {
  $dataset_id = NULL;
  if (!empty($fields) && ($dataset_name = variable_get('emf_graphicmail_lid_' . $lid . '_dataset', FALSE)) && ($dataset_array = variable_get('emf_graphicmail_datasets', FALSE))) {
    $dataset_id = $dataset_array[$dataset_name];
  }
  // API rate limiting workaround.
  sleep(3);
  return _emf_graphicmail_api_call('subscribeUser', array(
    $email,
    $lid,
    $fields,
    $dataset_id,
  ));
}

/**
 * Unsubscribe a user from a list.
 *
 * @param String $email
 *   E-mail address to unsubscribe.
 * @param String $lid
 *   List ID of the list to subscribe to.
 *
 * @return Boolean
 *   TRUE if user is subscribed. FALSE if not.
 */
function emf_graphicmail_api_unsubscribe($email, $lid) {
  $dataset_id = NULL;
  if (($dataset_name = variable_get('emf_graphicmail_lid_' . $lid . '_dataset', FALSE)) && ($dataset_array = variable_get('emf_graphicmail_datasets', FALSE))) {
    $dataset_id = $dataset_array[$dataset_name];
  }
  return _emf_graphicmail_api_call('unsubscribeUser', array(
    $email,
    $lid,
    $dataset_id,
  ));
}

/**
 * Fetch subscribed subscribers from API.
 *
 * @param Mixed $date
 *   If a string, should be in the format Y-m-d H:i:s',
 *   otherwise, a Unix timestamp.
 * @param String $lid
 *   List ID.
 *
 * @return Array
 *   List of subscriber lists.
 */
function emf_graphicmail_api_get_subscribers_subscribed($date = 0, $lid = NULL) {
  return _emf_graphicmail_api_call('getSubscribedSubscribers', array($lid));
}

/**
 * Fetch unsubscribed subscribers from API.
 *
 * @param Mixed $date
 *   If a string, should be in the format 'Y/m/d',
 *   otherwise, a Unix timestamp.
 * @param String $lid
 *   List ID.
 *
 * @return Array
 *   List of unsubscribed subscribers.
 */
function emf_graphicmail_api_get_subscribers_unsubscribed($date = 0, $lid = NULL) {
  return _emf_graphicmail_api_call('getUnsubscribedSubscribers', array($lid));
}

/**
 * Fetch subscribers from API.
 *
 * @param Mixed $date
 *   If a string, should be in the format of 'Y/m/d',
 *   otherwise, a Unix timestamp.
 * @param String $lid
 *   List ID.
 *
 * @return Array
 *   List of subscribers.
 */
function _emf_graphicmail_api_get_subscribers_active($date = 0, $lid = NULL) {
  return _emf_graphicmail_api_call('getSubscribedSubscribers', array($lid));
}

/**
 * Fetch lists from API.
 *
 * @return Array
 *   List of subscriber lists.
 */
function emf_graphicmail_api_get_lists() {
  $fields = array(
    'lid' => 'lid',
    'name_api' => 'name',
  );
  return _emf_graphicmail_api_get_array_call('getLists', array(), $fields, 'lid');
}

/**
 * Fetch datasets from API.
 *
 * @return Array
 *   List of datasets.
 */
function emf_graphicmail_api_get_datasets() {
  return _emf_graphicmail_api_call('getDatasets');
}

/**
 * Fetch custom fields for some list from API.
 *
 * @param Int $lid
 *   List ID of the list.
 *
 * @return Array
 *   List of custom fields.
 */
function emf_graphicmail_api_get_custom_fields($lid) {
  $fields = array(
    'key' => 'key',
    'name' => 'name',
  );
  $variable_name = 'emf_graphicmail_lid_' . $lid . '_dataset';
  if (($dataset_name = variable_get($variable_name, FALSE)) && ($dataset_array = variable_get('emf_graphicmail_datasets', FALSE))) {
    return _emf_graphicmail_api_get_array_call('getCustomFields', array($dataset_array[$dataset_name]), $fields, 'key');
  }
}

/**
 * Fetch newsletters from API.
 *
 * @return Array
 *   Multidimensional array of newsletters,
 *   with name, id and folder name
 */
function emf_graphicmail_api_get_newsletters() {
  return _emf_graphicmail_api_call('getNewsletters');
}

/**
 * Post a newsletter to the API.
 *
 * @param String $path
 *   The full path to the newsletter files.
 * @param String $filename
 *   The basename of the newsletter filename.
 *
 * @return String
 *   Message from API.
 */
function emf_graphicmail_post_newsletter($path, $filename) {
  return _emf_graphicmail_api_call('postNewsletter', array($path, $filename));
}


/**
 * Fetch system time.
 *
 * @return Int
 *   System time.
 */
function emf_graphicmail_api_get_system_time() {
  return time();
}

/**
 * Do API call.
 *
 * @param String $method
 *   The API method to call.
 * @param Array $params
 *   Parameters for the API call.
 *
 * @return Array
 *   API result array.
 */
function _emf_graphicmail_api_call($method, $params = array()) {
  // Fetch API key and client id.
  $username = variable_get('emf_graphicmail_api_username', '');
  $password = variable_get('emf_graphicmail_api_password', '');

  // If no API key or client id is specified, return FALSE.
  if (empty($username) || empty($password)) {
    return FALSE;
  }

  // Do API call.
  $gm = new Graphicmail($username, $password);
  try {
    $result = call_user_func_array(array($gm, $method), $params);
  }

  // If API result code is not 'ok', throw an error and write log.
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
    watchdog('emf_graphicmail', $e->getMessage(), array(), WATCHDOG_ERROR);
    return FALSE;
  }
  return $result;
}

/**
 * Do array API call and parse the results as an array.
 *
 * @param String $method
 *   The API method to call.
 * @param Array $params
 *   Parameters for the API call.
 * @param Array $fields
 *   Associative array for field mapping.
 *   Keys are local fields. Values are API fields.
 * @param String $key
 *   Local field that will be used to index result array.
 *
 * @return Array
 *   Result array.
 */
function _emf_graphicmail_api_get_array_call($method, $params, $fields, $key = NULL) {
  $items = array();

  // Do API call.
  $result = _emf_graphicmail_api_call($method, $params);
  if ($result === FALSE) {
    return FALSE;
  }

  // Converting API result to array.
  $api_fields = array_values($fields);

  if ($result[$api_fields[0]]) {
    // If we have only one list, convert it to an array.
    $object = new stdClass();
    foreach ($fields as $local_field => $api_field) {
      $object->{$local_field} = $result[$api_field];
      if ($key) {
        $items[$object->{$key}] = $object;
      }
      else {
        $items[] = $object;
      }
    }
  }
  else {
    if (!empty($result)) {
      foreach ($result as $item) {
        $object = new stdClass();
        foreach ($fields as $local_field => $api_field) {
          $object->{$local_field} = $item[$api_field];
          if ($key) {
            $items[$object->{$key}] = $object;
          }
          else {
            $items[] = $object;
          }
        }
      }
    }
  }
  return $items;
}

/**
 * Do scalar API call and parse the results as an array.
 *
 * @param String $method
 *   The API method to call.
 * @param Array $params
 *   Parameters for the API call.
 * @param Array $type
 *   API type used as index under $result['anyType'].
 * @param String $field
 *   Field.
 *
 * @return Array
 *   Result array.
 */
function _emf_graphicmail_api_get_scalar_array_call($method, $params, $type, $field) {
  $items = array();

  // Do API call.
  $result = _emf_graphicmail_api_call($method, $params);
  if ($result === FALSE) {
    return FALSE;
  }

  // Converting API result to array.
  if ($result['anyType'][$type][$field]) {

    // If we have only one subscriber, return it.
    $items[] = $result['anyType'][$type][$field];
  }
  else {
    if (!empty($result['anyType'][$type])) {
      foreach ($result['anyType'][$type] as $item) {
        $items[] = $item[$field];
      }
    }
  }
  return $items;
}

/**
 * Convert a UNIX timestamp to a date Graphicmail wants to receive.
 *
 * @param Int $timestamp
 *   The UNIX timestamp to convert.
 *
 * @return String
 *   The Date in Graphicmail format.
 */
function emf_graphicmail_api_unix_to_service_time($timestamp = 0) {
  if ($timestamp) {
    return date('Y/m/d', $timestamp);
  }
  return 0;
}
