<?php

/**
 * @file
 * Settings functions and callbacks.
 *
 * @author Mark Anslow
 */

/**
 * Implements hook_settings().
 *
 * @return Array
 *   System settings form
 */
function emf_graphicmail_settings() {
  $form = array();

  // Info.
  $form['emf_graphicmail_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('API Username'),
    '#description' => t('Your Graphicmail API Username. See Graphicmail\'s <a href="http://www.graphicmail.co.uk/api/documentation/">documentation</a> for more info.'),
    '#default_value' => variable_get('emf_graphicmail_api_username', ''),
    '#required' => TRUE,
  );
  $form['emf_graphicmail_api_password'] = array(
    '#type' => 'password',
    '#title' => t('API Password'),
    '#description' => t('Your Graphicmail Password. See Graphicmail\'s <a href="http://www.graphicmail.co.uk/api/documentation/">documentation</a> for more info.'),
    '#default_value' => variable_get('emf_graphicmail_api_password', ''),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
